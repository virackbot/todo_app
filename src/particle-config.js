const particlesConfig = {
 
  fpsLimit: 60,
  interactivity: {
    events: {
      onClick: {
        enable: true,
        mode: "push",
      },
      onHover: {
        enable: true,
        mode: "repulse",
      },
      resize: true,
    },
    modes: {
      bubble: {
        distance: 1000,
        duration: 4,
        opacity: 0.1,
        size: 40,
      },
      push: {
        quantity: 3,
      },
      repulse: {
        distance: 200,
        duration: 0.4,
      },
    },
  },
  
  particles: {
    color: {
      value: "#49759e",
    },
    links: {
      color: "#d9edff",
      distance: 120,
      enable: true,
      opacity: 0.5,
      width: 2,
    },
    collisions: {
      enable: true,
    },
    move: {
      direction: "none",
      enable: true,
      outMode: "bounce",
      random: false,
      speed: 1,
      straight: false,
    },
    number: {
      density: {
        enable: true,
        area: 800,
      },
      value: 80,
    },
    opacity: {
      value: 0.5,
    },
    shape: {
      type: "circle",
    },
    size: {
      random: true,
      value: 5,
    },
  },
  detectRetina: true,
  
  
};

export default particlesConfig