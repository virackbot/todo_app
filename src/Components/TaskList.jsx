import React, { useContext } from "react";
import { TodoListContext } from "../context/TodoListContext";
import Task from "./Task";

const TaskList = () => {
  const { tasks } = useContext(TodoListContext);

  return (
    <div>
      {tasks.length ? (
   
        <ul className="list">
          <li>
          {tasks.map(task => {
            return <Task task={task} key={task.id} />;
          })}
          </li>
        </ul>
   
      ) : (
        <div className="no-tasks">No To-Do</div>
      )}
    </div>
  );
};

export default TaskList;