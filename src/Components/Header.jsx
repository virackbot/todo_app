import React from 'react'

const Header = () => {
  return (
    <div className='header'>
      <h1>To Do List</h1>
      <hr/>
    </div>
  )
}

export default Header