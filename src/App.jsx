import TaskList from "./Components/TaskList";
import TaskForm from "./Components/TodoForm";
import Header from "./Components/Header";
import './App.css'
import ParticleBackground from "./bg";
import TodoListContextProvider from "./context/TodoListContext";

const App = () => {
  return (
    
    <TodoListContextProvider>
    <div className="container"  >    
   
      <div className="app-wrapper"  >
        <Header />
        <div className="main">
          
          <TaskForm />
          <TaskList />
        </div>
      </div>
      <ParticleBackground />  
    </div>
    
  </TodoListContextProvider>
      // <ParticleBackground />
    
    
  );
}


export default App;